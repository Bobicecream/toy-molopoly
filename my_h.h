#include <iostream>
#include <conio.h>
#include <windows.h>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#define U 72
#define R 77
#define D 80
#define L 75

using namespace std;

#ifndef myfunction
#define myfunction

enum printc{Red_F=1,Green_F,Blue_F,Red_B,Green_B,Blue_B,Coffe_F,Darkgreen_F,Darkblue_F,Coffe_B,Darkgreen_B,Darkblue_B,Yellow_F,Lightblue_F,Lightpink_F,Yellow_B,Lightblue_B,Lightpink_B,Tea_F,Darkgreenblue_F,Purple_F,Tea_B,Darkgreenblue_B,Purple_B};

extern class info f[60];
extern class players p[2];
extern bool isAIOn;
void introduce();
void gamemode();
void gotoxy(int,int);
int direction();
int rent(const int,const int);
int th();
int increase(const int,const int);
void clear(int,int,int);

template <class P>
void printandclear( bool f, int x, int y, P s, int l)
{
	if(f)
	{
		gotoxy(x,y);
		cout <<s;
	}
	else
		clear(x,y,l);
}


template <class T>
void color(int r,T read)
{
    //傳回一個標準輸出的handle
 HANDLE h = GetStdHandle ( STD_OUTPUT_HANDLE ); 
 WORD wOldColorAttrs;
 //建立console screen buffer的結構變數 
 CONSOLE_SCREEN_BUFFER_INFO csbiInfo;


 //傳回console視窗的資訊 ，儲存在之前建立的結構變數中 
 GetConsoleScreenBufferInfo(h, &csbiInfo);
 //保留console視窗的顏色屬性，以便在輸出後回覆 
 wOldColorAttrs = csbiInfo.wAttributes;


 //設定新的顏色 
 switch(r)
 {
 case 1:
	 SetConsoleTextAttribute ( h, FOREGROUND_RED | FOREGROUND_INTENSITY );
     break;
 case 2:
     SetConsoleTextAttribute ( h, FOREGROUND_GREEN | FOREGROUND_INTENSITY );
     break;
 case 3:
	 SetConsoleTextAttribute ( h, FOREGROUND_BLUE | FOREGROUND_INTENSITY );
     break;
 case 4:
	 SetConsoleTextAttribute ( h, BACKGROUND_RED | BACKGROUND_INTENSITY );
     break;
 case 5:
     SetConsoleTextAttribute ( h, BACKGROUND_GREEN | BACKGROUND_INTENSITY );
     break; 
 case 6:
     SetConsoleTextAttribute ( h, BACKGROUND_BLUE | BACKGROUND_INTENSITY );
     break;
 case 7:
	 SetConsoleTextAttribute ( h, FOREGROUND_RED  );
     break;
 case 8:
     SetConsoleTextAttribute ( h, FOREGROUND_GREEN  );
     break;
 case 9:
	 SetConsoleTextAttribute ( h, FOREGROUND_BLUE );
     break;
 case 10:
	 SetConsoleTextAttribute ( h, BACKGROUND_RED  );
     break;
 case 11:
     SetConsoleTextAttribute ( h, BACKGROUND_GREEN  );
     break; 
 case 12:
     SetConsoleTextAttribute ( h, BACKGROUND_BLUE  );
     break;
 case 13:
	 SetConsoleTextAttribute ( h, FOREGROUND_RED + FOREGROUND_GREEN | FOREGROUND_INTENSITY );
     break;
 case 14:
     SetConsoleTextAttribute ( h, FOREGROUND_GREEN + FOREGROUND_BLUE | FOREGROUND_INTENSITY);
     break; 
 case 15:
     SetConsoleTextAttribute ( h, FOREGROUND_BLUE + FOREGROUND_RED | FOREGROUND_INTENSITY );
     break;
 case 16:
	 SetConsoleTextAttribute ( h, BACKGROUND_RED + BACKGROUND_GREEN | BACKGROUND_INTENSITY );
     break;
 case 17:
     SetConsoleTextAttribute ( h, BACKGROUND_GREEN + BACKGROUND_BLUE | BACKGROUND_INTENSITY);
     break; 
 case 18:
     SetConsoleTextAttribute ( h, BACKGROUND_BLUE + BACKGROUND_RED | BACKGROUND_INTENSITY );
     break;
 case 19:
	 SetConsoleTextAttribute ( h, FOREGROUND_RED + FOREGROUND_GREEN  );
     break;
 case 20:
     SetConsoleTextAttribute ( h, FOREGROUND_GREEN + FOREGROUND_BLUE );
     break; 
 case 21:
     SetConsoleTextAttribute ( h, FOREGROUND_BLUE + FOREGROUND_RED  );
     break;
 case 22:
	 SetConsoleTextAttribute ( h, BACKGROUND_RED + BACKGROUND_GREEN  );
     break;
 case 23:
     SetConsoleTextAttribute ( h, BACKGROUND_GREEN + BACKGROUND_BLUE );
     break; 
 case 24:
     SetConsoleTextAttribute ( h, BACKGROUND_BLUE + BACKGROUND_RED );
     break;
 case 25:
	 SetConsoleTextAttribute ( h, BACKGROUND_GREEN + FOREGROUND_GREEN + FOREGROUND_INTENSITY );
	 break;

 default:
	 SetConsoleTextAttribute ( h, FOREGROUND_BLUE+FOREGROUND_RED+FOREGROUND_GREEN );
     break;
 }
 cout <<read;
 //回覆原來的顏色屬性 
 SetConsoleTextAttribute ( h, wOldColorAttrs);
}

#endif

