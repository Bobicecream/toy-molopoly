#include "Players.h"


int *pmo[3];
players::players(int o,char c)
{
	
	order=o;
	head=c;
	site=0;
	money=5000;
	for(int i=1;i<=2;i++)
	{
		if(order==i)
			pmo[i]=&money;
	}
	god=0;
	
}
void players::print()
{
	int an=0; 
	int z;
	static int cross=1;
	int s;
	 
	gotoxy(8,12);//show which player now
	if(order==1)//player1 
	{
		color(1,head);
		if(god==1)
			color(15,"G");
		/*for(int i=0;i<60;i++)
		{
			if(f[i].owner==1)
			an++;
		}
		cout <<an;
		an=0;*/
		cout <<" ";
	}
	else if(order==2)//player2
	{
		color(3,head);
		if(god==1)
			color(15,"G");
		cout <<" ";
	}	
	if(order==1 || !isAIOn) 
	{
		while(z=direction())
		{
#ifdef ERA //enable right array
			if(z==R)
			{
				cross=2;
				gotoxy(15,17);
				color(1," use");
				if(bag()==1)
				{
					int sstep;
					
					cin >>sstep;
					//s=sstep;
					s=(sstep>=1&&sstep<=12)?sstep:12;
					gotoxy(40,6);
					cout <<setw(2) <<s;
					break;
				}
			}
#endif
			if(z==L)
			{
				cross=1;
				gotoxy(15,17);
				color(1,"dice");
				s=th();//save how much step
				break;
			}
		}
	}
	else if(isAIOn && order==2)
	{
		cross=1;
		s=th();
	}
	//slowly move
	for(int i=1;i<=s;i++)
	{
		int s1=(site+i)%60,s2=(site+i-1)%60;
		f[s1].nop=order;	
		f[s1].slow(s2);
		if(f[s1].opportunity==2)
		{
			money+=1000;
			gotoxy(13,10);
			cout <<"+1000";
			Sleep(300);
			gotoxy(13,10);
			cout <<"     ";
		}
	}
	
	clear(15,17,4); // clear string dice
	site+=s;//original site add how much step equal current site
	site%=60;
	f[site].nop=order;
	f[site].print();
}

int players::gm()
{
	return money;
}
void players::buy()
{
	int z=1; 
	int sm=100*increase(f[site].l,f[site].el);
	int cross=1;//default buy house
	if(f[site].l>4)//limit house level
		cross=2;
	
	cout <<setfill(' ');
	if(f[site].opportunity==1)
		destiny();
	else if(f[site].opportunity==2)
		z=1;
	else if(f[site].owner==0 || f[site].owner==order)//if the house isn't bought or the house belong to me
	{
		if(order==2 && isAIOn)
		{
			z=0;
			if(cross==1)
			{
				int tmp;
				tmp = money - sm;
				if(tmp>0)
				{
					money = tmp;
					f[site].setl();
					f[site].owner=order;//set who the house belong to
					f[site].print();
				}
			}
		}
		else if(order==1 || !isAIOn)
		{
			gotoxy(6,7);
			cout <<setw(4) <<sm;
			
			//buy house or not
			gotoxy(6,8);
			color(23,"Yes");
			gotoxy(6,9);
			cout <<" No";
			
			while(z=direction())
			{
				if(z==U)
				{
					gotoxy(6,8);
					color(23,"Yes");
					gotoxy(6,9);
					cout <<" No";
					cross=(cross==2)?cross:1;//buy house if not reach max level
				}
				else if(z==D)
				{
					gotoxy(6,8);
					cout <<"Yes";
					gotoxy(6,9);
					color(23," No");
					cross=2;//don't buy
				}
				
				if(z==13)//enter
				{
					if(cross==1)
					{
						money-=sm; //how much to buy or increase
						f[site].setl();//increase house level
						f[site].owner=order;//set who the house belong to
						f[site].print();
					}
					break;
				}
			}
		}
		clear(6,8,3);
		clear(6,9,3);
	}
	else//if the house was bought
	{ 
		money-=rent( f[site].l,f[site].el )*200;//pay money for rent
		pay(f[site].owner);//who should I pay for
	}
}
		
		
void players::pay(const int py)
{
	int n=1;
	int sr=200*rent( f[site].l ,f[site].el);
	
	if(p[py-1].god==1)
		n=2;
	
	
	*pmo[py]+=sr*n;
	gotoxy(13,14);
	
	if(order==1)
		color(1,'A');
	if(order==2)
		color(3,'B');
	cout <<"-" <<setw(4) <<sr <<" ";
	if(py==1)
		color(1,'A');
	if(py==2)
		color(3,'B');
	cout <<"+" <<setw(4) <<sr*n;
	system("PAUSE");
	gotoxy(13,14);
	cout <<setw(39) <<" ";
}
void players::destiny()
{
	Sleep(500);
	int r=rand()%50;
	
	gotoxy(13,10);
	if(r==49)
	{
		money+=10000;
		cout <<"+10000";
	}
	if(r>=11)
	{
		money+=800;
		cout <<"+800";
	}
	if(r<=10)
	{
		*pmo[1]=*pmo[2]=(*pmo[1]+*pmo[2])/2;
		
		cout <<"average";
	}
	if(r==0)
	{
		money-=2000;
		cout <<" and less than other players 2000";
	}
	Sleep(1000);
	system("PAUSE");
	gotoxy(13,10);
	for(int i=1;i<=60;i++)
		cout <<" ";
}
int players::bag()
{
	gotoxy(2,30);
	int nth;
	
	cin >>nth;
	return nth;
}
